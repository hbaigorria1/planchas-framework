$('#sec01').owlCarousel({
    loop: false,
    margin: 10,
    dots: true,
    nav: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    items: 1,
});

$('#destacados').owlCarousel({
    loop: false,
    margin: 10,
    dots: true,
    nav: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:1,
        },
        1000:{
            items:5,
        }
    }
});
$('#secslide03').owlCarousel({
    loop: false,
    margin: 10,
    dots: true,
    nav: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    items: 1,
});

$('#slidevideos01').owlCarousel({
    loop: false,
    margin: 10,
    dots: true,
    nav: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    items: 1,
});

$('#slidevideos02').owlCarousel({
    loop: false,
    margin: 10,
    dots: true,
    nav: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    items: 1,
});

$('#slidevideos03').owlCarousel({
    loop: false,
    margin: 10,
    dots: true,
    nav: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    items: 1,
});

$('#slidevideos04').owlCarousel({
    loop: false,
    margin: 10,
    dots: true,
    nav: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    items: 1,
});

$( ".paso01-sec-01" ).click(function() {
  $('.modal-paso-01-sec-01').addClass("active-modal");
});
$( ".paso02-sec-01" ).click(function() {
  $('.modal-paso-02-sec-01').addClass("active-modal");
});

$( ".paso03-sec-01" ).click(function() {
  $('.modal-paso-03-sec-01').addClass("active-modal");
});

$( ".paso04-sec-01" ).click(function() {
  $('.modal-paso-04-sec-01').addClass("active-modal");
});

$( ".paso05-sec-01" ).click(function() {
  $('.modal-paso-05-sec-01').addClass("active-modal");
});

$( ".paso06-sec-01" ).click(function() {
  $('.modal-paso-06-sec-01').addClass("active-modal");
});

$( ".paso01-sec-02" ).click(function() {
  $('.modal-paso-01-sec-02').addClass("active-modal");
});

$( ".paso02-sec-02" ).click(function() {
  $('.modal-paso-02-sec-02').addClass("active-modal");
});

$( ".paso03-sec-02" ).click(function() {
  $('.modal-paso-03-sec-02').addClass("active-modal");
});

$( ".paso04-sec-02" ).click(function() {
  $('.modal-paso-04-sec-02').addClass("active-modal");
});
$( ".paso05-sec-02" ).click(function() {
  $('.modal-paso-05-sec-02').addClass("active-modal");
});

$( ".paso01-sec-03" ).click(function() {
  $('.modal-paso-01-sec-03').addClass("active-modal");
});
$( ".paso02-sec-03" ).click(function() {
  $('.modal-paso-02-sec-03').addClass("active-modal");
});
$( ".paso03-sec-03" ).click(function() {
  $('.modal-paso-03-sec-03').addClass("active-modal");
});
$( ".paso04-sec-03" ).click(function() {
  $('.modal-paso-04-sec-03').addClass("active-modal");
});

$( ".paso01-sec-04" ).click(function() {
  $('.modal-paso-01-sec-04').addClass("active-modal");
});
$( ".paso02-sec-04" ).click(function() {
  $('.modal-paso-02-sec-04').addClass("active-modal");
});
$( ".paso03-sec-04" ).click(function() {
  $('.modal-paso-03-sec-04').addClass("active-modal");
});


$( ".destacado-video-product" ).click(function() {
  $('.destacado-video-product-modal').addClass("active-modal");
});

$( ".close-modal" ).click(function() {
  $('.modal-paso-01-sec-01 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-02-sec-01 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-03-sec-01 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-04-sec-01 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-05-sec-01 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-01-sec-02 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-02-sec-02 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-03-sec-02 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-04-sec-02 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-05-sec-02 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-01-sec-03 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-02-sec-03 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-03-sec-03 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-04-sec-03 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-01-sec-04 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-02-sec-04 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-paso-03-sec-04 iframe').each(function(index) {
    $(this).attr('src', $(this).attr('src'));
    return false;
  });
  $('.modal-video').removeClass("active-modal");
});

$('.continue').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});
$('.back').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});

$( ".guia" ).click(function() {
  $('.paso-01-guia').addClass("active-paso-01");
});

$( ".guia-paso-02" ).click(function() {
  $('.paso-02-guia').addClass("active-paso-02");
});

$( ".guia-paso-03" ).click(function() {
  $('.paso-03-guia').addClass("active-paso-03");
});

$( "#sec001-tab" ).click(function() {
  $('.sec001-active-icon').show();
  $('.sec02-active-icon').hide();
  $('.sec03-active-icon').hide();
  $('.sec04-active-icon').hide();
});

$( "#sec02-tab" ).click(function() {
  $('.sec001-active-icon').hide();
  $('.sec02-active-icon').show();
  $('.sec03-active-icon').hide();
  $('.sec04-active-icon').hide();
});

$( "#sec03-tab" ).click(function() {
  $('.sec001-active-icon').hide();
  $('.sec02-active-icon').hide();
  $('.sec03-active-icon').show();
  $('.sec04-active-icon').hide();
});

$( "#sec04-tab" ).click(function() {
  $('.sec001-active-icon').hide();
  $('.sec02-active-icon').hide();
  $('.sec03-active-icon').hide();
  $('.sec04-active-icon').show();
});

function bootstrapTabControl(){
  var i, items = $('.nav-link'), pane = $('.tab-pane');
  // next
  $('.nexttab').on('click', function(){
      for(i = 0; i < items.length; i++){
          if($(items[i]).hasClass('active') == true){
              break;
          }
      }
      if(i < items.length - 1){
          // for tab
          $(items[i]).removeClass('active');
          $(items[i+1]).addClass('active');
          // for pane
          $(pane[i]).removeClass('show active');
          $(pane[i+1]).addClass('show active');
      }

  });
  // Prev
  $('.prevtab').on('click', function(){
      for(i = 0; i < items.length; i++){
          if($(items[i]).hasClass('active') == true){
              break;
          }
      }
      if(i != 0){
          // for tab
          $(items[i]).removeClass('active');
          $(items[i-1]).addClass('active');
          // for pane
          $(pane[i]).removeClass('show active');
          $(pane[i-1]).addClass('show active');
      }
  });
}
bootstrapTabControl();